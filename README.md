# ELP-BCO+FOD
Gitlab for Benoit COEUGNET and Frédéric ODDON.

## GO
We have implemented Dijkstra : TCP connection is done and the program is optimised. 

## JS
Read the readme for JS.

## ELM
Programme seems to work well according to our tests, read the associated readme.
