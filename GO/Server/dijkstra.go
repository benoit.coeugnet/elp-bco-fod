package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strconv"
	"time"
)

func printMatrix(A [][]int) {
	for i := 0; i < len(A); i++ {
		fmt.Println(A[i])
	}
}

func dijkstra(adjacencyMatrix [][]int, peak chan int, results chan<- []int) {
	for x := range peak {
		vectorDist := make([]int, len(adjacencyMatrix))
		for i := 0; i < len(adjacencyMatrix); i++ {
			vectorDist[i] = 100000
		}
		vectorDist[x] = 0
		visited := make([]bool, len(adjacencyMatrix))
		for i := 0; i < len(adjacencyMatrix); i++ {
			min := 100000
			minIndex := 0
			for j := 0; j < len(adjacencyMatrix); j++ {
				if !visited[j] && vectorDist[j] < min {
					min = vectorDist[j]
					minIndex = j
				}
			}
			visited[minIndex] = true
			for j := 0; j < len(adjacencyMatrix); j++ {
				if !visited[j] && adjacencyMatrix[minIndex][j] != 0 && vectorDist[minIndex] != 100000 && vectorDist[minIndex]+adjacencyMatrix[minIndex][j] < vectorDist[j] {
					vectorDist[j] = vectorDist[minIndex] + adjacencyMatrix[minIndex][j]
				}
			}
		}
		//fmt.Println(x, vectorDist)
		//adding the number of the peak to the vector
		vectorDist = append([]int{x}, vectorDist...)
		results <- vectorDist
	}
}

// go routine to take care of each client
func clientWork(c net.Conn, errorInReceive chan bool) {
	// Receive the size of the matrix from the client
	buf := bufio.NewReader(c)
	size, err := buf.ReadString('*')
	if err != nil {
		println("Read from the client failed:", err.Error())
		os.Exit(1)
	}
	// Receive the size of the matrix from the client
	N, _ := strconv.Atoi(string(size[:len(size)-1]))

	// Receive the matrix from the client
	A := make([][]int, N)
	for i := 0; i < N; i++ {
		A[i] = make([]int, N)
		for j := 0; j < N; j++ {
			number, err := buf.ReadString('*')
			if err != nil {
				println("Read from the client failed:", err.Error())
				errorInReceive <- true
			}
			A[i][j], _ = strconv.Atoi(number[:len(number)-1])
		}
	}

	jobs := make(chan int, N)
	results := make(chan []int, N)

	for w := 1; w <= 1; w++ {
		go dijkstra(A, jobs, results)
	}

	beginning := time.Now()
	for x := 0; x < N; x++ {
		jobs <- x
	}
	close(jobs)

	//Send the matrix to the client
	for i := 0; i < N; i++ {
		vectorDist := <-results
		for j := 0; j < N+1; j++ {
			_, err = c.Write([]byte(strconv.Itoa(vectorDist[j]) + "*"))
		}
	}

	ending := time.Now()
	fmt.Println(ending.Sub(beginning))
	errorInReceive <- false
}

func main() {
	// Le serv attend qu'un client lui donne une variable
	PORT := "localhost:6666"
	ln, err := net.Listen("tcp", PORT)
	if err != nil {
		println("Listen failed:", err.Error())
		os.Exit(1)
	}
	fmt.Println("Listening on port", PORT)

	for true {
		c, err := ln.Accept()
		if err != nil {
			println("Accept failed:", err.Error())
			break
		}
		fmt.Println("Client connected")

		errorInReceive := make(chan bool)
		go clientWork(c, errorInReceive)
		result := <-errorInReceive
		if result {
			fmt.Println("Error in receive")
		}
	}
}
