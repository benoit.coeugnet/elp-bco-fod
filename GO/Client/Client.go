package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"net"
	"os"
	"strconv"
)

var lineInt int

func createMatrix(size int) (A [][]int) {
	A = make([][]int, size)
	for i := range A {
		A[i] = make([]int, size)
	}
	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			if i == j { // Diagonale de la matrice d'adjacence
				A[i][j] = 0
			} else {
				if rand.Intn(3) == 1 {
					A[i][j] = rand.Intn(50)
				} else {
					A[i][j] = 0
				}
			}

		}
	}
	return A
}

func printMatrix(A [][]int) {
	for i := 0; i < len(A); i++ {
		fmt.Println(A[i])
	}
}

func main() {
	sizeOfTheMatrix := "200"
	servAddr := "localhost:6666"
	N, _ := strconv.Atoi(sizeOfTheMatrix)
	A := createMatrix(N)

	tcpAddr, err := net.ResolveTCPAddr("tcp", servAddr)
	if err != nil {
		println("ResolveTCPAddr failed:", err.Error())
		os.Exit(1)
	}

	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		println("Dial failed:", err.Error())
		os.Exit(1)
	}

	// Send the size of the matrix to the server
	_, err = conn.Write([]byte(sizeOfTheMatrix + "*"))
	if err != nil {
		println("Write to server failed:", err.Error())
		os.Exit(1)
	}

	// Send the matrix to the server
	for i := 0; i < N; i++ {
		for j := 0; j < N; j++ {
			_, err = conn.Write([]byte(strconv.Itoa(A[i][j]) + "*"))
			//fmt.Println("Write to server the value of the matrix =", A[i][j])
			if err != nil {
				println("Write to server failed:", err.Error())
				os.Exit(1)
			}
		}
	}

	//Receive the matrix from the server
	B := make([][]int, N)
	for i := 0; i < N; i++ {
		B[i] = make([]int, N)
	}
	buf := bufio.NewReader(conn)
	for i := 0; i < N; i++ {
		for j := 0; j < N+1; j++ {
			if j == 0 {
				lineStr, _ := buf.ReadString('*')
				lineInt, _ = strconv.Atoi(lineStr[:len(lineStr)-1])
			} else {
				number, err := buf.ReadString('*')
				//fmt.Println("Read number from the server =", number[:len(number)-1])
				if err != nil {
					println("Read from the server failed:", err.Error())
					os.Exit(1)
				}
				B[lineInt][j-1], _ = strconv.Atoi(number[:len(number)-1])
			}
		}
		fmt.Println(i, B[lineInt])
	}

	err = conn.Close()
	if err != nil {
		print("Close failed:", err.Error())
	}
}
