package main

import (
	"fmt"
	"time"
)

func cells(A, B [1000][1000]int, C [][]int, jobs <-chan int, size int, results chan<- int) {
	for x := range jobs {
		i := x / size
		j := x % size
		for k := 0; k < size; k++ {
			C[i][j] += A[i][k] * B[k][j]
		}
		results <- i + j
	}
}

func main() {
	const size = 1000
	const numJobs = size * size
	var A [size][size]int
	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			A[i][j] = i + j
		}
	}
	var B [size][size]int
	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			B[i][j] = i + j
		}
	}

	C := make([][]int, size)
	for i := range C {
		C[i] = make([]int, size)
	}

	jobs := make(chan int, numJobs)
	results := make(chan int, numJobs)

	for w := 1; w <= 5; w++ {
		go cells(A, B, C, jobs, size, results)
	}

	beginning := time.Now()
	for x := 0; x < numJobs; x++ {
		jobs <- x
	}
	close(jobs)

	for a := 0; a < numJobs; a++ {
		<-results
	}

	ending := time.Now()
	fmt.Println(ending.Sub(beginning))
	//fmt.Println(C)
}
