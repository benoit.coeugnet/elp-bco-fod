package main

import (
	"fmt"
	"time"
)

func worker(id int, jobs <-chan int, results chan<- int) {
	for j := range jobs {
		fmt.Println("hello world", id, j)
		time.Sleep(time.Second)
		results <- j * 2
	}
}

func main() {
	beginning := time.Now()

	const numJobs = 30
	jobs := make(chan int, numJobs)
	results := make(chan int, numJobs)

	for w := 1; w <= 3; w++ {
		go worker(w, jobs, results)
	}

	for j := 1; j <= numJobs; j++ {
		jobs <- j
	}
	close(jobs)

	for a := 1; a <= numJobs; a++ {
		<-results
	}

	ending := time.Now()
	elapsed := ending.Sub(beginning)
	fmt.Println(elapsed)
}
