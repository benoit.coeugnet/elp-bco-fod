from flask import Flask
from flask_cors import CORS
import http.server

app = Flask(__name__)
CORS(app)
CORS(app, resources={r"/*": {"origins": "*"}})

PORT = 6969
server_address = ("", PORT)

server = http.server.HTTPServer
handler = http.server.CGIHTTPRequestHandler
handler.cgi_directories = ["C:words.txt"] 

@app.route("/words.txt")
def words():
    with open("C:words.txt", "r") as f:
        content = f.read()
    return content

if __name__ == '__main__':
    print("Serveur actif sur le port :", PORT)
    httpd = server(server_address, handler)
    httpd.serve_forever()
