## Launching

To launch the program, follow the following instructions :
* With the Windows Command Prompt, go to [your folder]/src
* Type "elm make .\Main.elm" in the CMD
* Launch Main.html with the CMD or with any file explorer
* Run the Python file ScriptServ.py to start the server with the words
* We install an extension to our web browser to bypass the CORS error


## Explanations

* All definitions are displayed
* Press the check button to check if the word is the right one.
* Press show answer to show the answer
* Press next to display new definitions and continue playing (all will be reset)
* The answer will be hidden when the next button is pressed

