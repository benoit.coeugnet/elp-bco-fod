module WordGuessr exposing (..)

import Browser
import Html exposing (Html, button, div, h1, h2, input, li, p, text, ul, strong)
import Html.Attributes exposing (class, placeholder, style, value)
import Html.Events exposing (..)
import Http
import Json.Decode exposing (Decoder, field, int, list, map, map2, string)
import Random


-- MAIN
main =
  Browser.element
    { init = init
    , update = update
    , subscriptions = subscriptions
    , view = view
    }

-- MODEL
type alias Model =
    { wordsList : List String,
    wordsDefs : List Word,
    correctWord : String,
    wordIsCorrect : Bool,
    showAnswer : Bool,
    firstTime : Bool,
    placeHolder : String,
    inputAnswer : String }


init : () -> (Model, Cmd Msg)
init _ = ( {
    wordsList = [],
    wordsDefs = [],
    correctWord = "",
    wordIsCorrect = False,
    showAnswer = False,
    firstTime = True,
    placeHolder = "Enter your answer here",
    inputAnswer = ""}, getAllWords)


-- UPDATE
type Msg =
   GotWord (Result Http.Error (List Word)) | GotAllWords (Result Http.Error String) | RandomWord Int
    | CheckButton
    | Answer String
    | Next
    | ShowAnswer

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        GotAllWords result ->
            case result of
                Ok wordList ->
                    ({model | wordsList = String.split " " wordList  }, Random.generate RandomWord (Random.int 1 1000))

                Err _ ->
                    (model, Cmd.none)

        RandomWord index ->
            case (getElementAtIndex model.wordsList index) of
                  Nothing ->
                      (model, Cmd.none)
                  Just wordSelected ->
                      ({ model | correctWord = wordSelected}, getRandomWord wordSelected)

        Answer answer ->
            ({ model | inputAnswer = answer }, Cmd.none)

        CheckButton ->
            if model.inputAnswer == model.correctWord then
                ({ model | wordIsCorrect = True, firstTime = False}, Cmd.none)
            else
                ({ model | wordIsCorrect = False, firstTime = False }, Cmd.none)

        GotWord result ->
            case result of
                Ok words ->
                    ({ model | wordsDefs = words }, Cmd.none)

                Err _ ->
                    (model, Cmd.none)

        Next ->
            ({ model | wordIsCorrect = False, inputAnswer = "", firstTime = True, showAnswer = False }, Random.generate RandomWord (Random.int 1 1000))

        showAnswer ->
            ({ model | showAnswer = not model.showAnswer }, Cmd.none)


-- SUBSCRIPTIONS
subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none

-- VIEW
view : Model -> Html Msg
view model =
    div []
        [ h1 [] [ text "Word Guessr" ]
        , viewDefinition model
        ]


viewDefinition : Model -> Html Msg
viewDefinition model =
    div [ class "test" ]
    [
    p [] [ text "Welcome to Word Guessr : read the definitions below and try to guess which word they correspond to ! " ]
    , h2 [] [ text "Definitions" ]
    , p [] (List.map (\word ->
             ul [] (List.map (\meaning ->
                li [] [ text meaning.partOfSpeech, ul [] (List.map (\definition ->
                     li [] [ text definition.definition ]) meaning.definitions) ] )word.meanings) )model.wordsDefs)
    , h2 [] [ text "Guess the word" ]
    ,div [ class "input" ]
    [
    input [ placeholder model.placeHolder, value model.inputAnswer,onInput Answer ] []
    , button [ onClick CheckButton ] [ text "Check" ]
    , if not model.firstTime then
        div [ style "color" (if model.wordIsCorrect then "green" else "red") ] [ text (if model.wordIsCorrect then "Correct !" else "Incorrect !") ]
    else
        div [] [ text " " ]

    , if model.wordIsCorrect then
        div []  [ button [ onClick Next ] [ text "Next" ] ]
    else
        div [] [ text " "]
    ]
    , div []
    [
    button [ onClick ShowAnswer ] [ text (if model.showAnswer then "Hide answer" else "Show answer") ] ]
    , if model.showAnswer then
        div [] [ text "The right answer is : ", strong [] [ text model.correctWord ] ]
    else
        div [] [ text " " ]
    ]

-- HTTP
getElementAtIndex : List a -> Int -> Maybe a
getElementAtIndex list index =
    if index < 0 || index >= List.length list then
        Nothing
    else
        List.head (List.drop index list)

getAllWords : Cmd Msg
getAllWords =
    Http.get
      { url = "http://localhost:6969/words.txt"
      , expect = Http.expectString GotAllWords
      }


getRandomWord : String -> Cmd Msg
getRandomWord word =
  Http.get
    { url = "https://api.dictionaryapi.dev/api/v2/entries/en/" ++ word
    , expect = Http.expectJson GotWord wordsDecoder
    }

type alias Word = { meanings : List Meaning}
type alias Meaning = { partOfSpeech : String, definitions : List Definition }
type alias Definition = { definition : String}


wordsDecoder : Decoder (List Word)
wordsDecoder =
    (list eachWordDecoder)

eachWordDecoder : Decoder Word
eachWordDecoder =
    map Word
        (field "meanings" (list meaningDecoder))

meaningDecoder : Decoder Meaning
meaningDecoder =
    map2 Meaning
    (field "partOfSpeech" string)
    (field "definitions" (list definitionsDecoder))


definitionsDecoder : Decoder Definition
definitionsDecoder =
    map Definition
    (field "definition" string)
