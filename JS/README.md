## Program launching

Install following modules thanks to these commands :

    - npm install prompt-sync
    - npm install processes
    - npm install keypress

Type `node main.js` in shell to launch the program

## Custom commands

    - lp : list all current processes
    - keep <processID> : detach from the shell the process with the given process ID
        - If the process is already detached, the program warn the user that the process is already detached
    - bing [-k|-p|-c] <processID> : kill, pause or resume the given process
        - If a wrong process ID is given, the program warm the user that the process ID is incorrect
        - If no option or a wrong option is given, the program warn the user that a mandatory option is missing or is incorrect, and reminds them the correct syntax
    - exit : the program ends and returns back to the shell

## Features

    - All the classic shell commands are available
    - If nothing is written while pressing the Enter key, the program asks the user for another command
    - If an exclamation mark is present at the end of the command send, this command is executed in the background

## Notes

* We noticed that processes launched from a shell are automatically detached from it. We didn't know exactly why but it made coding certain commands useless, because the process was already working without them. Recently, we noticed that it happens because we are using a Debian distribution (debian 10.13) and sometimes WSL. We decided to do our project on linux and we don't have access to other distrubutions, so we stayed on Debian because we didn't have time to find another solution.
* We discovered that the prompt module takes over the keypress module while waiting for user input. This means that we cannot wait for a Ctrl+P input from the user to exit the program. We decided to not restart all of our programming to try another prompt solution and implemented the "exit" command to give the user a possibility to end the program. Our try to get some key input is in comment in our main.js.
