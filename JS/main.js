const prompt = require('prompt-sync')();
const { exec } = require('child_process');
const { kill } = require('process');
const process = require('process');
const keypress = require('keypress');
/* * * * Our try to get user keyboard input
const readline = require('readline');
readline.emitKeypressEvents(process.stdin);
process.stdin.setRawMode(true);
*/

input = "";
main();

function execCmd(cmd) {
    // console.log(`Command executed : ${cmd}`)
    exec(cmd, (error, stdout, stderr) => {
        
        if (error) {
            console.log(`error: ${error.message}`);
        }

        if (stderr) {
            console.log(`stderr: ${stderr}`);
        }

        console.log(`stdout:\n${stdout}`);
        main();
    });
}

function main() {
    const input = prompt('>>> ');

    /* * * * Our try to get user keyboard input
    keypress(process.stdin);
    process.stdin.on('keypress', (str, key) => {
        if (key.ctrl && key.name === 'a') {
          process.exit();
        } else {
          console.log(`You pressed the "${str}" key`);
          console.log();
          console.log(key);
          console.log();
        }
        console.log('Press any key...');
    });
    */

    console.log(`Command input : ${input}`);
    

    if (input == "exit") {
        process.kill(process.pid, "SIGINT");
    } else if (input == "") {
        main();
    } else if (input.slice(0,5) == "keep ") {

        processID = input.slice(5);
        console.log(`Process ${processID} already in background or wrong process ID`);
        
        main();
        
    } else if (input.slice(0,4) == "bing") {
        option = input.slice(5,7);
        processID = input.substring(8);
        if (option == "-k") {
            try {
                process.kill(processID, "SIGKILL")
                console.log(`Killed process ${processID} successfully`)
            } catch (error) {
                console.log(`Error : Couldn't kill process ${processID} : doesn't exist ?`)
            }
        } else if (option == "-p") {
            try {
                process.kill(processID, "SIGSTOP")
                console.log(`Paused process ${processID} successfully`)
            } catch (error) {
                console.log(`Error : Couldn't stop process ${processID} : doesn't exist ?`)
            }
        } else if (option == "-c") {
            try {
                process.kill(processID, "SIGCONT")
                console.log(`Resumed process ${processID} successfully`)

            } catch (error) {
                console.log(`Error : Couldn't resume process ${processID} : doesn't exist ?`)
            }
        } else {
            console.log(`Mandatory option missing or incorrect`)
            console.log(`bing full syntax : bing [-k|-p|-c] <processId>`)
        }
        main();
        
    } else if (input.slice(-1) == "!") {
        execCmd(input.slice(0, -1)+"&");
    } else if (input == "lp") {
        execCmd("ps -ax");
    } else {
        execCmd(input);
    }
    
}
